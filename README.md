# SMITIDvisu (R HTMLwidgets)

## Time and Date

Time and more specificaly Date use the ISO format [ISA_8601](https://fr.wikipedia.org/wiki/ISO_8601).

> "YYYY-MM-DDTHH:MM:SSZ" -> "2018-02-08T10:22:00Z"

## Install

```bash
R CMD build .
R CMD INSTALL SMITIDvisu*.tar.gz
```

## htmlwidget

All methods below implements htmlwidget.
They can be use in R console, in R markdown docs and shiny apps.

To export in standalone mode a htmlwidget in a browser:
```r
my_htmlwidget <- method(values)
htmlwidgets::saveWidget(my_htmlwidget, "demofile.html")
browseURL("demofile.html")
```

## Transmission Tree (a time tree)

A Transmission Tree reprensents host (as node) and pathogen infection (as edge) over the time.
TransmissionTree function need two parameters :
- nodes as a data.frame with ID, status and time in columns
- edges as a data.frame with unique ID, source, target and time in columns

```r
library(SMITIDvisu)
data(transmissiontree)
transmissionTree(tt.nodes, tt.edges, nodes.color = list("default"="black","Inf"="red"))
```

## Map Transmission Tree

```r
library(SMITIDvisu)
data(transmissiontree)
maptt(tt.events, multipleValuesByTime = c('infectedby', 'probabilities'))
```

## Host Time Line

A Host Time Line reprensents informations about a host over the time.
timeLine function need one parameter :
- items as a data.frame with level, label, ID, timestart and timeend

```r
library(SMITIDvisu)
data(hostline)
timeLine(hostline, title="113", color=list("infected"="red","offspring"="green","alive"="blue","inf"="orange","dead"="black","Obs"="purple"))
```

## Variants plot
The Variants plot (mstVariant) draw variants genotypes distances as a graph at a time or at several times.

```r
library(SMITIDvisu)
data(st)
mstVariant(st.dist113_2,st.prop113_2)
# With variant proportions over time
mstVariant(st.dist113_all, st.prop113_all, st.listTimeProp113)
```


## Authors

Jean-Francois Rey <jean-francois.rey@inra.fr>

## Bugs

Need coffee
